from Cat_mouse import catAndmouse
import unittest

class distance_cat_mouse(unittest.TestCase):
 def test_give_distance(self):
    cat_a = 2
    cat_b = 3 
    mouse = 4
    cat_win = catAndmouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance2(self):
    cat_a = 10
    cat_b = 5 
    mouse = 16
    cat_win = catAndmouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance3(self):
    cat_a = 5.6
    cat_b = 4.5 
    mouse = 3.1
    cat_win = catAndmouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance4(self):
    cat_a = -4
    cat_b = 1 
    mouse = 9
    cat_win = catAndmouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance5(self):
    cat_a = -6
    cat_b = -4
    mouse = -5
    cat_win = catAndmouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)

 def test_give_distance6(self):
    cat_a = 10
    cat_b = 6
    mouse = 8
    cat_win = catAndmouse(cat_a,cat_b,mouse)
    self.assertTrue(cat_win)
