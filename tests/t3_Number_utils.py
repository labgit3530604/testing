from Number_utils import number_list
import unittest

class PrimeListTest(unittest.TestCase):
 def test_give_1_2_3_is_prime(self):
    prime_list = [1, 2, 3]
    is_prime = number_list(prime_list)
    self.assertTrue(is_prime)

 def test_give_2_3_7_is_prime(self):
    prime_list = [2, 3, 7]
    is_prime = number_list(prime_list)
    self.assertTrue(is_prime)

 def test_give_2_4_8_is_prime(self):
    prime_list = [2, 4, 8]
    is_prime = number_list(prime_list)
    self.assertTrue(is_prime)

 def test_give_minus5_minus6_1_is_prime(self):
    prime_list = [-5, -6, 1]
    is_prime = number_list(prime_list)
    self.assertTrue(is_prime)

 def test_give_1_0_9_is_prime(self):
    prime_list = [1, 0, 9]
    is_prime = number_list(prime_list)
    self.assertTrue(is_prime)

