def fizz_buzz(x):
    if x % 3 == 0 and x % 5 == 0:
        return "FizzBuss"
    elif x % 3 == 0:
        return "Fizz"
    elif x % 5 == 0:
        return "Bus"
    else:
        return "Not FizzBuss"